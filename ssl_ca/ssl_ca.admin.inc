<?php

/**
 * SSL Certificate Authority Administration
 */

function ssl_ca_admin_form() {
  $form = array();

  // Add other config bits here, like keysize
  
  $form['ssl_ca_days_valid'] = array(
    '#type' => 'textfield',
    '#title' => t('Certificate Length'),
    '#description' => t('How long certificates are valid, in days.'),
    '#default_value' => variable_get('ssl_ca_days_valid', 365),
  );
  
  return system_settings_form($form);
} 
 
function ssl_ca_admin_cacert_form() {
  $form = array();

  $form['ssl_ca_cacert'] = array(
    '#type' => 'file',
    '#title' => t('Certificate Authority Cert'),
    '#description' => t('PKCS #12 bundle with CA cert and private key.  These will be used to sign user\'s certs.'),
  );

  $form['ssl_ca_p12pass'] = array(
    	'#type' => 'password',
    	'#title' => t('PKCS#12 Bundle Pass Phrase'),
    	'#description' => t('Pass prase for the PKCS #12 file above.  Leave blank if the bundle is not encrypted.'),
  );
  
  $form['ssl_ca_certpass'] = array(
    	'#type' => 'password',
    	'#title' => t('CA Key Pass Phrase'),
    	'#description' => t('Pass prase for the CA Key in the bundle above.  Leave blank if the keyfile is not encrypted.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#validate'][] = 'ssl_ca_admin_cacert_form_validate';
  $form['#submit'][] = 'ssl_ca_admin_cacert_form_submit';

  return $form;
}

function ssl_ca_admin_cacert_form_validate($form, &$form_state) {
  $file = file_save_upload('ssl_ca_cacert', array(
    'ssl_ca_validate_file_is_pkcs12' => array($form_state['values']['ssl_ca_p12pass']), 
    'file_validate_extensions' => array(),
  ));
  // If the file passed validation:
  if ($file) {
    // Move the file, into the Drupal file system
    if ($file = file_move($file, 'private://')) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t('Failed to write the uploaded file to the site\'s file folder.'));
    }
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }
}

function ssl_ca_admin_cacert_form_submit($form, &$form_state) {
  $file = $form_state['storage']['file'];
  // We are done with the file, remove it from storage.
  unset($form_state['storage']['file']);
  // Make the storage of the file permanent
  $file->status = FILE_STATUS_PERMANENT;
  // Save file status.
  file_save($file);

  variable_set('ssl_ca_cacert', $file->fid);
  variable_set('ssl_ca_p12pass', $form_state['values']['ssl_ca_p12pass']);
  variable_set('ssl_ca_certpass', $form_state['values']['ssl_ca_certpass']);
}

function ssl_ca_admin_dn_form() {
  $form = array();
 
  $cacert = ssl_ca_load_ca_cert();
  $cert = openssl_x509_parse($cacert['cert']);

  $form['ssl_ca_add_extra_dn'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Subject Data'),
    '#description' => t('Add extra Subject Data to the generated cert\'s distingushed name. Defaults to the subject data in the CA Cert.'),
    '#default_value' => variable_get('ssl_ca_add_extra_dn', 0),
  );
 
  $form['ssl_ca_site_country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country Name'),
    '#default_value' => variable_get('ssl_ca_site_country', ''),
    '#hint' => $cert['subject']['C'],
    '#states' => array(
      'enabled' => array(
        ':input["ssl_ca_add_extra_dn"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  $form['ssl_ca_site_state'] = array(
    '#type' => 'textfield',
    '#title' => t('State or Province'),
    '#default_value' => variable_get('ssl_ca_site_country', ''),
    '#hint' => $cert['subject']['ST'],
    '#states' => array(
      'enabled' => array(
        ':input["ssl_ca_add_extra_dn"]' => array('checked' => TRUE),
      ),
    ),
  );
   
  $form['ssl_ca_site_locality'] = array(
    '#type' => 'textfield',
    '#title' => t('Locality'),
    '#default_value' => variable_get('ssl_ca_site_locality', ''),
    '#hint' => $cert['subject']['L'],
    '#states' => array(
      'enabled' => array(
        ':input["ssl_ca_add_extra_dn"]' => array('checked' => TRUE),
      ),
    ),
  );
 
  $form['ssl_ca_org_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Orginization Name'),
    '#default_value' => variable_get('ssl_ca_org_name', ''),
    '#hint' => $cert['subject']['O'],
    '#states' => array(
      'enabled' => array(
        ':input["ssl_ca_add_extra_dn"]' => array('checked' => TRUE),
      ),
    ),
  );
   
  $form['ssl_ca_unit_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Organizational Unit Name'),
    '#default_value' => variable_get('ssl_ca_unit_name', ''),
    '#hint' => $cert['subject']['OU'],
    '#states' => array(
      'enabled' => array(
        ':input["ssl_ca_add_extra_dn"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  return system_settings_form($form);
}

function ssl_ca_validate_file_is_pkcs12($file, $password) {
  $errors = array();
  $certs = array();

  $pkcs12 = file_get_contents($file->uri);
  if (openssl_pkcs12_read($pkcs12, $certs, $password) === FALSE) {
    $errors[] = t('Could not open PKCS #12 file.  Wrong password or not a PKCS #12 file.');
  }

  return $errors;
}

