= Installation =

This module has some significant server side configuration requirements that will make it impossible to run on hosting services that cost less than a pizza a month.

Your server must support the PHP OpenSSL extension.  For most Linux distributions, the OpenSSL extension is included by default with the PHP 5.x packages.  You can verify the OpenSSL extension is installed and active by enabling the devel module and going to devel/phpinfo.

Your server must also require the client provide an SSL certificate for user/login/ssl.  The configurations described below will help you set that up.


== Setting Up SSL Authentication with Apache 2.x == 
This is a sample VirtualHost configuration that enables client SSL authentication.

<VirtualHost *:443>
  ServerName example.org

  DocumentRoot /var/www/example.org

  # Enable SSL 
  SSLEngine On

  # These configure the server's regular SSL certificate, 
  # like you would do for any SSL enabled site.
  # This identifies the server to the user
  SSLCertificateFile /etc/ssl/sites/example_org_crt.pem
  SSLCertificateKeyFile /etc/ssl/sites/example_org_key
  SSLCertificateChainFile /etc/ssl/ca-certs/ca_bundle.pem

  # User certificates must be signed with this cert.
  SSLCACertificateFile /etc/ssl/ca-certs/example_ca.crt

  # Stock Drupal directory settings
  <Directory /var/www/example.org>
    Options +FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>

  # For most pages, don't request a client certificate.
  SSLVerifyClient none

  # Don't uncomment this for production.  It opens bad security holes.
  # But you may need it for debugging your server.
#  SSLInsecureRenegotiation On

  # If you're Drupal site is under a subdirectory (e.g., http://example.org/drupal7),
  # then add the prefix to this location. (e.g., /drupal7/usr/login/ssl)
  # This is the path the SSL Auth module uses to get the user's SSL certificate.
  <Location /user/login/ssl>
    # Ensure authentcation is done over SSL
    SSLRequireSSL

    # Require the client to send us his cert.
    SSLVerifyClient require

    # Allow for several intermediate certs
    SSLVerifyDepth 5

    # Pass data required by the SSL Auth module to Drupal
    SSLOptions +StdEnvVars +ExportCertData

    # Require the Client cert to be signed by a signing cert with this DN.
#    SSLRequire %{SSL_CLIENT_I_DN} eq "/C=US/ST=District of Columbia/L=Washignton/O=Example Org/OU=Certificate Authority/CN=example.org/emailAddress=ssl@example.org"

    # Pass data required by the SSL Auth module to Drupal
    # FakeBasicAuth turns the DN into a username for BasicAuth
    # See http://httpd.apache.org/docs/2.2/mod/mod_ssl.html#ssloptions
#    SSLOptions +StdEnvVars +ExportCertData +FakeBasicAuth

    # Configure the FakeBasicAuth
#    AuthName "Example Auth"
#    AuthType Basic
#    AuthUserFile auth/cert.passwd
#    Require valid-user
  </Location>
</VirtualHost>
